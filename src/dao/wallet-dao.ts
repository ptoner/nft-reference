import { inject, injectable } from "inversify"
import { Pool } from "pg"
import { Wallet } from "../dto/postgres/wallet"

@injectable()
class WalletDao {

    constructor(
        @inject("pool") private pool: Pool
    ) { }

    async create(wallet: Wallet) {

        await this.pool.query(`
            INSERT INTO wallet(
                "walletAddress",
                "network"
            )
            VALUES ($1, $2);
        `, [
            wallet.walletAddress,
            wallet.network
        ])

    }

    async read(walletAddress: string, network:string): Promise<Wallet> {

        let result = await this.pool.query(`SELECT * from wallet WHERE "walletAddress" = $1 and "network" = $2`, [walletAddress, network])

        return result.rows[0]

    }


    async delete(walletAddress: string, network:string) {
        await this.pool.query(`DELETE from wallet WHERE "walletAddress" = $1 and "network" = $2`, [walletAddress, network])
    }

    

}

export {
    WalletDao
}