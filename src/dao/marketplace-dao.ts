import { inject, injectable } from "inversify"
import { Pool } from "pg"
import { Attribute } from "../dto/postgres/attribute"
import { Marketplace } from "../dto/postgres/marketplace"

@injectable()
class MarketplaceDao {

    constructor(
        @inject("pool") private pool: Pool
    ) { }

    async create(marketplace: Marketplace)  {

        let result = await this.pool.query(`
            INSERT INTO marketplace (
                "name"
            )
            VALUES ($1) RETURNING "id";
        `, [
            marketplace.name
        ])

        marketplace.id = result.rows[0].id

    }

    async read(id:number): Promise<Attribute> {

        let result = await this.pool.query(`SELECT * from marketplace WHERE "id" = $1`, [id])

        return result.rows[0]

    }


}

export {
    MarketplaceDao
}