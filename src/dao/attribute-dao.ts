import { inject, injectable } from "inversify"
import { Pool } from "pg"
import { Attribute } from "../dto/postgres/attribute"
import { Wallet } from "../dto/postgres/wallet"

@injectable()
class AttributeDao {

    constructor(
        @inject("pool") private pool: Pool
    ) { }

    async create(attribute: Attribute)  {

        let result = await this.pool.query(`
            INSERT INTO attribute(
                "name"
            )
            VALUES ($1) RETURNING "id";
        `, [
            attribute.name
        ])

        attribute.id = result.rows[0].id

    }

    async read(id:number): Promise<Attribute> {

        let result = await this.pool.query(`SELECT * from attribute WHERE "id" = $1`, [id])

        return result.rows[0]

    }


}

export {
    AttributeDao
}