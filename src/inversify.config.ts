import { Container } from "inversify";

import { WalletDao } from "./dao/wallet-dao";
import { DatasetService } from "./service/dataset-service";

import { Pool } from "pg";
import { AttributeDao } from "./dao/attribute-dao";
import { MarketplaceDao } from "./dao/marketplace-dao";

const MLBStatsAPI = require('mlb-stats-api');

const {BigQuery} = require('@google-cloud/bigquery');
const {Storage} = require('@google-cloud/storage');

let container

function getContainer() {

    if (container) return container

    container = new Container()

    // function bigquery() {
    
    //     const bqOptions = {
    //         keyFilename: '../keys/mlb-data-engine-3e055f9e0c27.json',
    //         projectId: 'mlb-data-engine',
    //       };
          
    //     // Create a client
    //     return new BigQuery(bqOptions)
    
    // }

    
    function storage() {
        // return new Storage({
        //     keyFilename: '../keys/mlb-data-engine-3e055f9e0c27.json',
        //     projectId: 'mlb-data-engine',
        //   })
    }
    
    function pool() {
        
        return new Pool({
            user: "postgres",
            host: "localhost",
            database: "mlb_stats",
            password: "mysweetdbpassword",
            port: 5432
        })
    
    }
    
    
    container.bind("datasetId").toConstantValue("mlb_stats")
    // container.bind("data_directory").toConstantValue("../data")
    // container.bind("bigquery").toConstantValue(bigquery())
    container.bind("storage").toConstantValue(storage())
    container.bind("pool").toConstantValue(pool())
    
    container.bind(DatasetService).toSelf().inSingletonScope()
    
    container.bind(WalletDao).toSelf().inSingletonScope()
    container.bind(AttributeDao).toSelf().inSingletonScope()
    container.bind(MarketplaceDao).toSelf().inSingletonScope()

    return container
}




export {
    getContainer
}