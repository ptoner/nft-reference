interface Marketplace {

    id?:number
    name:string

    lastUpdated?:Date
    dateCreated?:Date

}

export {
    Marketplace
}