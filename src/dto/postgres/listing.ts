import { Marketplace } from "./marketplace";
import { Nft } from "./nft";

interface Listing {

    id?:number 

    nft:Nft
    marketplace:Marketplace

    startPrice?:number
    endPrice:number
    currency:string

    endDate?:Date

    lastUpdated?:Date
    dateCreated?:Date

}

export {
    Listing
}