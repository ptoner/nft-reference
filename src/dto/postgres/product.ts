interface Product {

    id?:number
    name:string

    lastUpdated?:Date
    dateCreated?:Date

}

export {
    Product
}