import { Product } from "./product";
import { Wallet } from "./wallet";

interface Nft {

    id?:number 
    serialNumber:number

    wallet:Wallet
    product:Product

    minted:Date

    lastUpdated?:Date
    dateCreated?:Date

}

export {
    Nft
}