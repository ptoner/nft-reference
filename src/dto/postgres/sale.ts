import { Listing } from "./listing";
import { Nft } from "./nft";
import { Wallet } from "./wallet";

interface Sale {

    id?:number
    nft:Nft

    buyer:Wallet 

    listing:Listing

    lastUpdated?:Date
    dateCreated?:Date

}

export {
    Sale
}