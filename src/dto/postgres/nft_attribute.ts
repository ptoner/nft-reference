import { Attribute } from "./attribute";
import { Nft } from "./nft";

interface NftAttribute {

    nft:Nft
    attribute:Attribute

}

export {
    NftAttribute
}