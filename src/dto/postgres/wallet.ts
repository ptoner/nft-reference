interface Wallet {

    id?:number
    
    walletAddress:string
    network:string

    lastUpdated?:Date
    dateCreated?:Date

}

export {
    Wallet
}