import { Nft } from "./nft";
import { Wallet } from "./wallet";

interface Transfer {

    id?:number
    transactionHash:string

    nft:Nft

    sender:Wallet
    receiver:Wallet

    lastUpdated?:Date
    dateCreated?:Date

}

export {
    Transfer
}