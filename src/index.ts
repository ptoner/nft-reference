import "core-js/stable"
import "regenerator-runtime/runtime"
import "reflect-metadata"

import { getContainer } from "./inversify.config"
import { DatasetService } from "./service/dataset-service"


export {
    DatasetService,
    getContainer
}
