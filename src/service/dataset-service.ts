import { inject, injectable } from "inversify";

@injectable()
class DatasetService {

    constructor(
        @inject("bigquery") private bigquery,
        @inject("datasetId") private datasetId
    ) { }

    async createDataset() {

        // Specify the geographic location where the dataset should reside
        const options = {
            location: 'US',
        };

        // Create a new dataset
        const [dataset] = await this.bigquery.createDataset(this.datasetId, options)

        console.log(`Dataset ${dataset.id} created.`)
    }

    async deleteDataset() {

        const dataset = this.bigquery.dataset(this.datasetId)

        // Delete the dataset and its contents
        await dataset.delete({ force: true })

        console.log(`Dataset ${dataset.id} deleted.`);

    }


    async createTables() {

        let schemas = [
            {
                name: "game",
                schema: [
                    { name: 'gamePk', type: 'INT64', mode: 'REQUIRED' },
                    { name: 'rotogrinderId', type: 'INT64' },
                    { name: 'status', type: 'STRING' },
                    { name: 'season', type: 'STRING' },
                    { name: 'type', type: 'STRING' },
                    { name: 'gameDate', type: 'DATETIME' },
                    { name: 'officialDate', type: 'DATE' },
                    { name: 'dayNight', type: 'STRING' },

                    { name: 'venue', type: 'RECORD', fields: [
                        { name: 'id', type: 'INT64'},
                        { name: 'name', type: 'STRING'},
                        { name: 'link', type: 'STRING'},
                    ]},

                    { name: 'homeTeamId', type: 'INT64', mode: 'REQUIRED' },
                    { name: 'awayTeamId', type: 'INT64', mode: 'REQUIRED' },

                    { name: 'homeProbablePitcherId', type: 'INT64' },
                    { name: 'awayProbablePitcherId', type: 'INT64'},

                    { name: 'homeStartingPitcherId', type: 'INT64' },
                    { name: 'awayStartingPitcherId', type: 'INT64'},

                    { name: 'homeBattingOrder', type: 'INT64', mode: 'REPEATED'},
                    { name: 'awayBattingOrder', type: 'INT64', mode: 'REPEATED'},

                    { name: 'hitters', type: 'INT64', mode: 'REPEATED'},
                    { name: 'pitchers', type: 'INT64', mode: 'REPEATED'},

                    { name: 'winningPitcherId', type: 'INT64'},
                    { name: 'losingPitcherId', type: 'INT64'},
                    { name: 'savePitcherId', type: 'INT64'},

                    { name: 'homeVegasProjectedRunsScored', type: 'INT64'},
                    { name: 'awayVegasProjectedRunsScored', type: 'INT64'},

                    { name: 'awayVegasStrikeouts', type: 'INT64'},
                    { name: 'homeVegasStrikeouts', type: 'INT64'},

                    { name: 'homeMoneyLine', type: 'INT64'},
                    { name: 'awayMoneyLine', type: 'INT64'},

                    { name: 'parkFactor', type: 'INT64'},
                    { name: 'tempFactor', type: 'INT64'},
                    { name: 'windFactor', type: 'INT64'},

                    { name: 'awayLineupPopulated', type: 'BOOL'},
                    { name: 'awayLineupConfirmed', type: 'BOOL'},
                    { name: 'awayLineupUnconfirmed', type: 'BOOL'},

                    { name: 'homeLineupPopulated', type: 'BOOL'},
                    { name: 'homeLineupConfirmed', type: 'BOOL'},
                    { name: 'homeLineupUnconfirmed', type: 'BOOL'},

                    { name: 'weather', type: 'RECORD', fields: [
                        { name: 'condition', type: 'STRING'},
                        { name: 'temp', type: 'INT64'},
                        { name: 'wind', type: 'STRING'},
                    ]},

                    { name: 'awayTeamRuns', type: 'INT64'},
                    { name: 'homeTeamRuns', type: 'INT64'},

                    { name: 'awayTeamHits', type: 'INT64'},
                    { name: 'homeTeamHits', type: 'INT64'},

                    { name: 'homeTeamErrors', type: 'INT64'},
                    { name: 'awayTeamErrors', type: 'INT64'},

                    { name: 'preAllstar', type: 'BOOL'}

                ]
            },
            {
                name: "play",
                schema: [

                    { name: 'id', type: 'STRING', mode: 'REQUIRED' },

                    { name: 'result', type: 'RECORD', fields: [
                        { name: 'type', type: 'STRING'},
                        { name: 'event', type: 'STRING'},
                        { name: 'eventType', type: 'STRING'},
                        { name: 'description', type: 'STRING'},
                        { name: 'rbi', type: 'STRING'},
                        { name: 'awayScore', type: 'STRING'},
                        { name: 'homeScore', type: 'STRING'}
                    ]},

                    { name: 'about', type: 'RECORD', fields: [
                        { name: 'atBatIndex', type: 'INT64'},
                        { name: 'halfInning', type: 'INT64'},
                        { name: 'isTopInning', type: 'BOOL'},
                        { name: 'inning', type: 'INT64'},
                        { name: 'startTime', type: 'DATETIME'},
                        { name: 'endTime', type: 'DATETIME'},
                        { name: 'isComplete', type: 'BOOL'},
                        { name: 'isScoringPlay', type: 'BOOL'},
                        { name: 'hasReview', type: 'BOOL'},
                        { name: 'hasOut', type: 'BOOL'},
                    ]},

                    { name: 'count', type: 'RECORD', fields: [
                        { name: 'balls', type: 'INT64'},
                        { name: 'strikes', type: 'INT64'},
                        { name: 'out', type: 'INT64'}
                    ]},

                    { name: 'matchup', type: 'RECORD', fields: [

                        { name: 'batter', type: 'RECORD', fields: [
                            { name: 'strikes', type: 'INT64'}
                        ]},
                        { name: 'batSide', type: 'RECORD', fields: [
                            { name: 'code', type: 'STRING'}
                        ]},
                        { name: 'pitcher', type: 'RECORD', fields: [
                            { name: 'id', type: 'INT64'}
                        ]},
                        { name: 'pitchHand', type: 'RECORD', fields: [
                            { name: 'code', type: 'STRING'}
                        ]},
                    ]},

                    { name: 'runners', type: 'RECORD', mode: "REPEATED", fields: [

                        { name: 'movement', type: 'RECORD', fields: [
                            { name: 'originBase', type: 'STRING'},
                            { name: 'start', type: 'STRING'},
                            { name: 'end', type: 'STRING'},
                            { name: 'outBase', type: 'STRING'},
                            { name: 'isOut', type: 'STRING'},
                            { name: 'outNumber', type: 'STRING'},
                        ]},

                        { name: 'details', type: 'RECORD', fields: [
                            { name: 'event', type: 'STRING'},
                            { name: 'eventType', type: 'STRING'},
                            { name: 'movementReason', type: 'STRING'},

                            { name: 'runner', type: 'RECORD', fields: [
                                { name: 'id', type: 'INT64'}
                            ]},

                            { name: 'responsiblePitcher', type: 'RECORD', fields: [
                                { name: 'id', type: 'INT64'}
                            ]},

                            { name: 'isScoringEvent', type: 'BOOL'},
                            { name: 'rbi', type: 'BOOL'},
                            { name: 'earned', type: 'BOOL'},
                            { name: 'teamEarned', type: 'BOOL'},
                            { name: 'playIndex', type: 'INT64'},

                        ]},

                        { name: 'credits', type: 'RECORD', mode: "REPEATED", fields: [

                            { name: 'player', type: 'RECORD', fields: [
                                { name: 'id', type: 'INT64'}
                            ]},

                            { name: 'position', type: 'RECORD', fields: [
                                { name: 'code', type: 'INT64'},
                                { name: 'name', type: 'STRING'},
                                { name: 'abbreviation', type: 'STRING'}
                            ]},

                            { name: 'credit', type: 'STRING'}

                        ]},

                        
                    ]},

                    { name: 'playEvents', type: 'RECORD', mode: "REPEATED", fields: [

                        { name: 'details', type: 'RECORD', fields: [

                            { name: 'call', type: 'RECORD', fields: [
                                { name: 'code', type: 'STRING'},
                                { name: 'description', type: 'STRING'}
                            ]},

                            { name: 'description', type: 'STRING'},
                            { name: 'event', type: 'STRING'},
                            { name: 'eventType', type: 'STRING'},
                            { name: 'awayScore', type: 'INT64'},
                            { name: 'homeScore', type: 'INT64'},
                            { name: 'code', type: 'STRING'},
                            { name: 'ballColor', type: 'STRING'},
                            { name: 'trailColor', type: 'INT64'},
                            { name: 'isInPlay', type: 'BOOL'},
                            { name: 'isStrike', type: 'BOOL'},
                            { name: 'isBall', type: 'BOOL'},

                            { name: 'type', type: 'RECORD', fields: [
                                { name: 'code', type: 'STRING'},
                                { name: 'description', type: 'STRING'}
                            ]},

                            { name: 'isScoringPlay', type: 'BOOL'},
                            { name: 'hasReview', type: 'BOOL'},

                        ]},

                        { name: 'count', type: 'RECORD', fields: [
                            { name: 'balls', type: 'INT64'},
                            { name: 'strikes', type: 'INT64'},
                            { name: 'out', type: 'INT64'}
                        ]},

                        { name: 'pitchData', type: 'RECORD', fields: [
                            { name: 'startSpeed', type: 'NUMERIC'},
                            { name: 'endSpeed', type: 'NUMERIC'},
                            { name: 'strikeZoneTop', type: 'NUMERIC'},
                            { name: 'strikeZoneBottom', type: 'NUMERIC'},
                            { name: 'coordinates', type: 'RECORD', fields: [
                                { name: 'aY', type: 'NUMERIC'},
                                { name: 'aZ', type: 'NUMERIC'},
                                { name: 'pfxX', type: 'NUMERIC'},
                                { name: 'pfxZ', type: 'NUMERIC'},
                                { name: 'pX', type: 'NUMERIC'},
                                { name: 'pZ', type: 'NUMERIC'},
                                { name: 'vX0', type: 'NUMERIC'},
                                { name: 'vY0', type: 'NUMERIC'},
                                { name: 'vZ0', type: 'NUMERIC'},
                                { name: 'x', type: 'NUMERIC'},
                                { name: 'y', type: 'NUMERIC'},
                                { name: 'x0', type: 'NUMERIC'},
                                { name: 'y0', type: 'NUMERIC'},
                                { name: 'z0', type: 'NUMERIC'},
                                { name: 'aX', type: 'NUMERIC'},
                            ]},
                            { name: 'breaks', type: 'RECORD', fields: [
                                { name: 'breakAngle', type: 'NUMERIC'},
                                { name: 'spinRate', type: 'NUMERIC'},
                                { name: 'spinDirection', type: 'NUMERIC'}
                            ]},
                            { name: 'zone', type: 'NUMERIC'},
                            { name: 'plateTime', type: 'NUMERIC'},
                            { name: 'extension', type: 'NUMERIC'}

                        ]},

                        { name: 'hitData', type: 'RECORD', fields: [

                            { name: 'launchSpeed', type: 'NUMERIC'},
                            { name: 'launchAngle', type: 'NUMERIC'},
                            { name: 'totalDistance', type: 'NUMERIC'},
                            { name: 'trajectory', type: 'STRING'},
                            { name: 'hardness', type: 'STRING'},
                            { name: 'location', type: 'STRING'},
                            

                            { name: 'coordinates', type: 'RECORD', fields: [
                                { name: 'coordX', type: 'NUMERIC'},
                                { name: 'coordY', type: 'NUMERIC'}
                            ]}

                        ]},

                        { name: 'index', type: 'INT64'},
                        { name: 'playId', type: 'STRING'},
                        { name: 'pitchNumber', type: 'INT64'},
                        { name: 'startTime', type: 'DATETIME'},
                        { name: 'endTime', type: 'DATETIME'},
                        { name: 'isPitch', type: 'BOOL'},
                        { name: 'type', type: 'STRING'},
                        { name: 'player', type: 'RECORD', fields: [
                            { name: 'id', type: 'INT64'},
                        ]}

                    ]},



                ]
            }
        ]


        
        for (let table of schemas) {

            const [createdTable] = await this.bigquery
                .dataset(this.datasetId)
                .createTable(table.name, {
                    schema: table.schema,
                    location: 'US'
                })

        }




        // Create a new table in the dataset


    }


    async getDataset() {

        try {
            const [dataset] = await this.bigquery.dataset(this.datasetId).get()
            return dataset
        } catch (ex) {
            // console.log("Error getting dataset")
        }

    }


}


export {
    DatasetService
}