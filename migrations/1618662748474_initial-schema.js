/* eslint-disable camelcase */

exports.shorthands = undefined;

exports.up = pgm => {
    
    pgm.createTable("wallet", {

        "id": { type:"bigserial", unique: true, primaryKey: true, notNull: true },

        "walletAddress": { type:"varchar(500)", notNull: true },
        "network": { type:"varchar(100)", notNull: true },

        "lastUpdated": { type:"date", default: pgm.func('current_timestamp') },
        "dateCreated": { type:"date", default: pgm.func('current_timestamp') },

    })


    //Add unique key for walletAddress and network
    pgm.addConstraint("wallet", "wallet_address_network", {
        unique: ["walletAddress", "network"]
    })



    pgm.createTable("product", {

        "id": { type:"bigserial", unique: true, primaryKey: true, notNull: true },

        "name": { type:"varchar(500)", notNull: true },

        "lastUpdated": { type:"date", default: pgm.func('current_timestamp') },
        "dateCreated": { type:"date", default: pgm.func('current_timestamp') },

    })

    



    pgm.createTable("nft", {

        "id": { type:"bigserial", unique: true, primaryKey: true, notNull: true },
        "serialNumber": { type:"bigint", notNull: true },
        "productId": { type:"bigint", notNull: true },

        "walletId": { type:"bigint", notNull: true },

        "minted": { type:"date", notNull: true },

        "lastUpdated": { type:"date", default: pgm.func('current_timestamp') },
        "dateCreated": { type:"date", default: pgm.func('current_timestamp') },

    })

    //Foreign key for wallet_id
    pgm.addConstraint("nft", "nft_wallet_id", {
        foreignKeys: [{
            columns: ["walletId"],
            references: 'wallet ("id")'
        }]
    })

    //Foreign key for product_id
    pgm.addConstraint("nft", "nft_product_id", {
        foreignKeys: [{
            columns: ["productId"],
            references: 'product ("id")'
        }]
    })

    //Add unique key for productId and serialNumber
    pgm.addConstraint("nft", "nft_product_id_serial_number", {
        unique: ["productId", "serialNumber"]
    })



    pgm.createTable("attribute", {

        "id": { type:"bigserial", unique: true, primaryKey: true, notNull: true },

        "name": { type:"varchar(500)", notNull: true, unique: true },
        
        "lastUpdated": { type:"date", default: pgm.func('current_timestamp') },
        "dateCreated": { type:"date", default: pgm.func('current_timestamp') },

    })


    pgm.createTable("nft_attribute", {

        "id": { type:"bigserial", unique: true, primaryKey: true, notNull: true },

        "nftId": { type:"bigint", notNull: true },
        "attributeId": { type:"bigint", notNull: true },
        
        "lastUpdated": { type:"date", default: pgm.func('current_timestamp') },
        "dateCreated": { type:"date", default: pgm.func('current_timestamp') },

    })

    //Foreign key for nft_id
    pgm.addConstraint("nft_attribute", "nft_attribute_nft_id", {
        foreignKeys: [{
            columns: ["nftId"],
            references: 'nft ("id")'
        }]
    })

    //Foreign key for attribute
    pgm.addConstraint("nft_attribute", "nft_attribute_attribute_id", {
        foreignKeys: [{
            columns: ["attributeId"],
            references: 'attribute ("id")'
        }]
    })

        
    pgm.createTable("marketplace", {

        "id": { type:"bigserial", unique: true, primaryKey: true, notNull: true },

        "name": { type:"varchar(500)", notNull: true, unique: true },

        "lastUpdated": { type:"date", default: pgm.func('current_timestamp') },
        "dateCreated": { type:"date", default: pgm.func('current_timestamp') },

    })



    pgm.createTable("listing", {

        "id": { type:"bigserial", unique: true, primaryKey: true, notNull: true },

        "nftId": { type:"bigint", notNull: true },
        "marketplaceId": { type:"bigint", notNull: true },

        "startPrice": { type:"numeric(50, 18)" },
        "endPrice": { type:"numeric(50, 18)", notNull: true },
        "currency": { type:"varchar(100)", notNull: true },

        "endDate": { type:"date" },

        "lastUpdated": { type:"date", default: pgm.func('current_timestamp') },
        "dateCreated": { type:"date", default: pgm.func('current_timestamp') },

    })

    //Foreign key for nft_id
    pgm.addConstraint("listing", "listing_nft_id", {
        foreignKeys: [{
            columns: ["nftId"],
            references: 'nft ("id")'
        }]
    })

    pgm.addConstraint("listing", "listing_marketplace_id", {
        foreignKeys: [{
            columns: ["marketplaceId"],
            references: 'marketplace ("id")'
        }]
    })




    pgm.createTable("sale", {

        "id": { type:"bigserial", unique: true, primaryKey: true, notNull: true },

        "nftId": { type:"bigint", notNull: true },
        "buyerId": { type:"bigint", notNull: true },
        "listingId": { type:"bigint", notNull: true },

        "lastUpdated": { type:"date", default: pgm.func('current_timestamp') },
        "dateCreated": { type:"date", default: pgm.func('current_timestamp') },

    })

    //Foreign key for nft_id
    pgm.addConstraint("sale", "sale_nft_id", {
        foreignKeys: [{
            columns: ["nftId"],
            references: 'nft ("id")'
        }]
    })

    //Foreign key for buyer_id
    pgm.addConstraint("sale", "sale_buyer_id", {
        foreignKeys: [{
            columns: ["buyerId"],
            references: 'wallet ("id")'
        }]
    })

    //Foreign key for listing_id
    pgm.addConstraint("sale", "sale_listing_id", {
        foreignKeys: [{
            columns: ["listingId"],
            references: 'listing ("id")'
        }]
    })
    

    pgm.createTable("transfer", {

        "id": { type:"bigserial", unique: true, primaryKey: true, notNull: true },
        "transactionHash": { type: "varchar(500)", notNull: true },

        "nftId": { type:"bigint", notNull: true },
        "senderId": { type:"bigint", notNull: true },
        "receiverId": { type:"bigint", notNull: true },

        "lastUpdated": { type:"date", default: pgm.func('current_timestamp') },
        "dateCreated": { type:"date", default: pgm.func('current_timestamp') },

    })

    //Foreign key for nft_id
    pgm.addConstraint("transfer", "transfer_nft_id", {
        foreignKeys: [{
            columns: ["nftId"],
            references: 'nft ("id")'
        }]
    })

    //Foreign key for sender_id
    pgm.addConstraint("transfer", "transfer_sender_id", {
        foreignKeys: [{
            columns: ["senderId"],
            references: 'wallet ("id")'
        }]
    })


    //Foreign key for receiver_id
    pgm.addConstraint("transfer", "transfer_receiver_id", {
        foreignKeys: [{
            columns: ["receiverId"],
            references: 'wallet ("id")'
        }]
    })





};

exports.down = pgm => {};
