import "reflect-metadata"

import { Container } from "inversify";
import { DatasetService } from "../src/service/dataset-service";
import { WalletDao } from "../src/dao/wallet-dao";

const { BigQuery } = require('@google-cloud/bigquery');
const { Storage } = require('@google-cloud/storage');

import { Pool } from "pg";
import { AttributeDao } from "../src/dao/attribute-dao";
import { MarketplaceDao } from "../src/dao/marketplace-dao";


let container


async function getTestContainer() {

    if (container) return container

    container = new Container()

    function bigquery() {

        //TODO: SHOULD CONNECT TO A TEST DATABASE

        const bqOptions = {
            keyFilename: './keys/mlb-data-engine-3e055f9e0c27.json',
            projectId: 'mlb-data-engine',
        };

        // Create a client
        return new BigQuery(bqOptions)

    }

    function storage() {
        return new Storage({
            keyFilename: './keys/mlb-data-engine-3e055f9e0c27.json',
            projectId: 'mlb-data-engine',
        })
    }


    function pool() {

        return new Pool({
            user: "postgres",
            host: "localhost",
            database: "test_nft_reference",
            password: "mysweetdbpassword",
            port: 5432
        })

    }

    container.bind("datasetId").toConstantValue("test_mlb_stats")
    container.bind("data_directory").toConstantValue("./data")
    container.bind("bigquery").toConstantValue(bigquery())
    container.bind("storage").toConstantValue(storage())
    container.bind("pool").toConstantValue(pool())


    container.bind(DatasetService).toSelf().inSingletonScope()
    container.bind(WalletDao).toSelf().inSingletonScope()
    container.bind(AttributeDao).toSelf().inSingletonScope()
    container.bind(MarketplaceDao).toSelf().inSingletonScope()


    let p:Pool = container.get("pool")

    //Clear out existing data
    await p.query("DELETE FROM attribute")
    await p.query("DELETE FROM listing")
    await p.query("DELETE FROM marketplace")
    await p.query("DELETE FROM nft_attribute")
    await p.query("DELETE FROM nft")
    await p.query("DELETE FROM product")
    await p.query("DELETE FROM sale")
    await p.query("DELETE FROM transfer")
    await p.query("DELETE FROM wallet")


    // let datasetService: DatasetService = container.get(DatasetService)

    //Delete existing dataset if it exists
    // let dataset = await datasetService.getDataset()
    // if (dataset) {
    //     await datasetService.deleteDataset()
    // }

    // //Create dataset
    // await datasetService.createDataset()

    // //Create tables
    // await datasetService.createTables()


    return container

}




export {
    getTestContainer
}