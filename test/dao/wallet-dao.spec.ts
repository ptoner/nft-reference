import { getTestContainer } from "../inversify.config"

const assert = require('assert')

import { WalletDao } from "../../src/dao/wallet-dao"
import { Wallet } from "../../src/dto/postgres/wallet"


let container

describe('WalletDao', async () => {    

    let walletDao: WalletDao


    before('Main setup', async () => {

        container = await getTestContainer()

        walletDao = container.get(WalletDao)

    })

    it("should create and read a wallet", async () => {
        
        //Arrange
        let wallet:Wallet = {
            walletAddress: "0x123",
            network: "ethereum"
        }

        //Act
        await walletDao.create(wallet)

        //Assert
        let readWallet:Wallet = await walletDao.read("0x123", "ethereum")

        assert.strictEqual(readWallet.walletAddress, "0x123")
        assert.strictEqual(readWallet.network, "ethereum")

    })


    it("should not be able to insert a duplicate wallet", async () => {
        
        //Arrange
        let wallet:Wallet = {
            walletAddress: "0x123",
            network: "ethereum"
        }

        //Act
        try {
            await walletDao.create(wallet)
            assert.fail("Did not throw exception")
        } catch(ex) {
            assert.strictEqual(ex.message, 'duplicate key value violates unique constraint "wallet_address_network"')
        }

    })


    it("should delete a wallet", async () => {
        
        //Act
        await walletDao.delete("0x123", "ethereum")

        //Assert
        let readWallet:Wallet = await walletDao.read("0x123", "ethereum")

        assert.strictEqual(readWallet, undefined)

    })




    
})


