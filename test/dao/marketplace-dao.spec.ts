import { getTestContainer } from "../inversify.config"

const assert = require('assert')

import { MarketplaceDao } from "../../src/dao/marketplace-dao"
import { Wallet } from "../../src/dto/postgres/wallet"
import { Attribute } from "../../src/dto/postgres/attribute"
import { Marketplace } from "../../src/dto/postgres/marketplace"


let container

describe('MarketplaceDao', async () => {    

    let marketplaceDao: MarketplaceDao


    before('Main setup', async () => {

        container = await getTestContainer()

        marketplaceDao = container.get(MarketplaceDao)

    })

    it("should create and read a marketplace", async () => {
        
        //Arrange
        let marketplace:Marketplace = {
            name: "OpenSea"
        }

        //Act
        await marketplaceDao.create(marketplace)

        //Assert
        let readMarketplace:Marketplace = await marketplaceDao.read(marketplace.id)

        assert.strictEqual(readMarketplace.name, "OpenSea")

    })


    it("should not be able to insert a duplicate marketplace", async () => {
        
        //Arrange
        let marketplace:Marketplace = {
            name: "OpenSea"
        }

        //Act
        try {
            await marketplaceDao.create(marketplace)
            assert.fail("Did not throw exception")
        } catch(ex) {
            assert.strictEqual(ex.message, 'duplicate key value violates unique constraint "marketplace_name_key"')
        }

    })




    
})


