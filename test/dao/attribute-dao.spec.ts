import { getTestContainer } from "../inversify.config"

const assert = require('assert')

import { AttributeDao } from "../../src/dao/attribute-dao"
import { Wallet } from "../../src/dto/postgres/wallet"
import { Attribute } from "../../src/dto/postgres/attribute"


let container

describe('AttributeDao', async () => {    

    let attributeDao: AttributeDao


    before('Main setup', async () => {

        container = await getTestContainer()

        attributeDao = container.get(AttributeDao)

    })

    it("should create and read an attribute", async () => {
        
        //Arrange
        let attribute:Attribute = {
            name: "Big Yellow Hat"
        }

        //Act
        await attributeDao.create(attribute)

        //Assert
        let readAttribute:Attribute = await attributeDao.read(attribute.id)

        assert.strictEqual(readAttribute.name, "Big Yellow Hat")

    })


    it("should not be able to insert a duplicate attribute", async () => {
        
        //Arrange
        let attribute:Attribute = {
            name: "Big Yellow Hat"
        }

        //Act
        try {
            await attributeDao.create(attribute)
            assert.fail("Did not throw exception")
        } catch(ex) {
            assert.strictEqual(ex.message, 'duplicate key value violates unique constraint "attribute_name_key"')
        }

    })




    
})


