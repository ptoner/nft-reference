import path from 'path'
import webpack from 'webpack'
const nodeExternals = require('webpack-node-externals')


const babelLoader = {
  loader: 'babel-loader',
  options: {
    cacheDirectory: false,
    presets: ['@babel/preset-env']
  }
}


export default {
  entry: './src/index.ts',
  externals: [nodeExternals()],
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: '/node_modules/',
        loader: 'ts-loader',
      }
    ],
  },
  resolve: {
    extensions: ['*', '.js', '.jsx', '.tsx', '.ts'],
    alias: {
      buffer: 'buffer'
    }
  },
  output: {
    filename: 'mlb.js',
    libraryTarget: 'umd',
    globalObject: 'this',
    path: path.resolve(__dirname, 'dist')
  },
  plugins: [

    new webpack.ProvidePlugin({
      Buffer: ['buffer', 'Buffer'],
    })

  ]
}





