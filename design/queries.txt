select 
	p."hitterId",
	p."gamePk",
	
	count(*) as pa, -- plate appearances
	
	count(*) filter (where 
		p.event != 'Intent Walk' AND
		p.event != 'Hit By Pitch' AND
		p.event != 'Sac Bunt' AND 
		p.event != 'Sac Fly' AND 
		p.event != 'Sac Fly DP' AND 
		p.event != 'Walk' AND
		p.event != 'Catcher Interference' AND
		p.event != 'Runner Out' AND
		p.event != 'Ejection'
	) as "atBats", -- at bats
	
	count(*) filter (where
		p.event = 'Single' OR
		p.event = 'Double' OR
		p.event = 'Triple' OR
		p.event = 'Home Run'
	) as "hits", -- hits
	
	count(*) filter (where 
		p.event = 'Single'
	) as "singles", -- singles
	
	count(*) filter (where 
		p.event = 'Double'
	) as "doubles", -- doubles
	
	count(*) filter (where 
		p.event = 'Triple'
	) as "triples", -- triples
	
	count(*) filter (where 
		p.event = 'Home Run'
	) as "homeRuns", -- home runs	
	
	(SELECT 
	 	count(*) 
	 FROM runner r 
	 where 
	  	r."runnerId" = p."hitterId" and
	 	r."gamePk" = p."gamePk" and
	 	r."isScoringEvent" = true) "runs", 
	
	sum(p.rbi) as "rbi",
	
	count(*) filter (where 
		p.event = 'Walk' OR
		p.event = 'Intent Walk'
	) as "bb",
	
	(SELECT 
	 	count(*) filter (where 
			 r.event = 'Stolen Base 2B' OR
			 r.event = 'Stolen Base 3B' OR
			 r.event = 'Stolen Base Home' 
		)  
	 FROM runner r 
	 where 
	  	r."runnerId" = p."hitterId" and
	 	r."gamePk" = p."gamePk" 
	) as "sb",
	
	(SELECT 
	 	count(*) filter (where 
			 r.event = 'Caught Stealing 2B' OR
			 r.event = 'Caught Stealing 3B' OR
			 r.event = 'Caught Stealing Home' 
		)
	 FROM runner r 
	 where 
	  	r."runnerId" = p."hitterId" and
	 	r."gamePk" = p."gamePk" 
	) as "cs",
	
	count(*) filter (where 
		p.event = 'Hit By Pitch' 
	) as "hbp",
	
	count(*) filter (where 
		p.event = 'Strikeout' OR
		p.event = 'Strikeout - DP'
	) as "so",
	
	-- skip left on base for now
	
	count(*) filter (where 
		p.event = 'Sac Bunt'
	) as "sacBunts",
	
	count(*) filter (where 
		p.event = 'Sac Fly'
	) as "sacFlys",
	
	count(*) filter (where 
		(
			-- Filter for outs
			p.event = 'Batter Interference' OR
			p.event = 'Bunt Groundout' OR
			p.event = 'Bunt Lineout' OR
			p.event = 'Bunt Pop Out' OR
			p.event = 'Fan interference' OR
			p.event = 'Fielders Choice Out' OR
			p.event = 'Flyout' OR
			p.event = 'Forceout' OR
			p.event = 'Grounded Into DP' OR
			p.event = 'Groundout' OR
			p.event = 'Lineout' OR
			p.event = 'Pop Out' OR
			p.event = 'Sac Bunt' OR
			p.event = 'Sac Fly' OR
			p.event = 'Sac Fly DP' OR
			p.event = 'Sacrifice Bunt DP' OR
			p.event = 'Strikeout' OR
			p.event = 'Strikeout - DP' OR
			p.event = 'Triple Play' 
		) AND
		(
			-- Filter for ground balls
			p.event = 'Bunt Groundout' OR
			p.event = 'Grounded Into DP' OR
			p.event = 'Groundout' OR
			p.event = 'Fielders Choice' OR
			p.event = 'Fielders Choice Out' OR
			p.event = 'Force Out' OR
			p.event = 'Sac Bunt' OR
			p.event = 'Sacrifice Bunt DP' OR
			p.description LIKE '% ground ball %' OR
			p.description LIKE '% grounds out %' OR
			p.description LIKE '% grounds into %'		
		)
					 
	) as "groundOuts",
	
	
	count(*) filter (where 
		(
			-- Filter for outs
			p.event = 'Batter Interference' OR
			p.event = 'Bunt Groundout' OR
			p.event = 'Bunt Lineout' OR
			p.event = 'Bunt Pop Out' OR
			p.event = 'Fan interference' OR
			p.event = 'Fielders Choice Out' OR
			p.event = 'Flyout' OR
			p.event = 'Forceout' OR
			p.event = 'Grounded Into DP' OR
			p.event = 'Groundout' OR
			p.event = 'Lineout' OR
			p.event = 'Pop Out' OR
			p.event = 'Sac Bunt' OR
			p.event = 'Sac Fly' OR
			p.event = 'Sac Fly DP' OR
			p.event = 'Sacrifice Bunt DP' OR
			p.event = 'Strikeout' OR
			p.event = 'Strikeout - DP' OR
			p.event = 'Triple Play' 
		) AND
		(
			-- Filter for fly balls
			p.event = 'Flyout' OR
			p.event = 'Pop Out' OR
			p.event = 'Sac Fly' OR
			p.event = 'Sac Fly DP' OR
			p.description LIKE '% bunt pops %' OR
			p.description LIKE '% pops out %' OR
			p.description LIKE '% pops into %' OR
			p.description LIKE '% flies out %' OR
			p.description LIKE '% fly ball %' OR
			p.description LIKE '% flies into %' 
		)
					 
	) as "flyOuts",
	
	
	count(*) filter (where 
		(
			-- Filter for outs
			p.event = 'Batter Interference' OR
			p.event = 'Bunt Groundout' OR
			p.event = 'Bunt Lineout' OR
			p.event = 'Bunt Pop Out' OR
			p.event = 'Fan interference' OR
			p.event = 'Fielders Choice Out' OR
			p.event = 'Flyout' OR
			p.event = 'Forceout' OR
			p.event = 'Grounded Into DP' OR
			p.event = 'Groundout' OR
			p.event = 'Lineout' OR
			p.event = 'Pop Out' OR
			p.event = 'Sac Bunt' OR
			p.event = 'Sac Fly' OR
			p.event = 'Sac Fly DP' OR
			p.event = 'Sacrifice Bunt DP' OR
			p.event = 'Strikeout' OR
			p.event = 'Strikeout - DP' OR
			p.event = 'Triple Play' 
		) AND
		(
			-- Filter for line drives
			p.event = 'Lineout' OR
			p.event = 'Bunt Lineout' OR
			p.event = 'Bunt Pop Out' OR
			p.description LIKE '% line drive %' OR
			p.description LIKE '% lines out %' OR
			p.description LIKE '% lines into %' 
		)
					 
	) as "lineOuts",
	
	count(*) filter (where 
		(
			-- Filter for ground balls
			p.event = 'Bunt Groundout' OR
			p.event = 'Grounded Into DP' OR
			p.event = 'Groundout' OR
			p.event = 'Fielders Choice' OR
			p.event = 'Fielders Choice Out' OR
			p.event = 'Force Out' OR
			p.event = 'Sac Bunt' OR
			p.event = 'Sacrifice Bunt DP' OR
			p.description LIKE '% ground ball %' OR
			p.description LIKE '% grounds out %' OR
			p.description LIKE '% grounds into %'		
		)
					 
	) as "groundBalls",
	
	count(*) filter (where 
		(
			-- Filter for fly balls
			p.event = 'Flyout' OR
			p.event = 'Pop Out' OR
			p.event = 'Sac Fly' OR
			p.event = 'Sac Fly DP' OR
			p.description LIKE '% bunt pops %' OR
			p.description LIKE '% pops out %' OR
			p.description LIKE '% pops into %' OR
			p.description LIKE '% flies out %' OR
			p.description LIKE '% fly ball %' OR
			p.description LIKE '% flies into %' 
		)
					 
	) as "flyBalls",	


	count(*) filter (where 
		(
			-- Filter for line drives
			p.event = 'Lineout' OR
			p.event = 'Bunt Lineout' OR
			p.event = 'Bunt Pop Out' OR
			p.description LIKE '% line drive %' OR
			p.description LIKE '% lines out %' OR
			p.description LIKE '% lines into %' 
		)
					 
	) as "lineDrives",
	
	count(*) filter (where 
		p.event = 'Grounded Into DP'
	) as "gidp",
	
	(SELECT 
	 	count(*) filter (where(
			rc.credit = 'f_putout'
		))
	 FROM runner_credit rc 
	 where 
	  	rc."playerId" = p."hitterId" and
	 	rc."gamePk" = p."gamePk") "po",
		
		
	(SELECT 
	 	count(*) filter (where(
			rc.credit = 'f_assist'
		))
	 FROM runner_credit rc 
	 where 
	  	rc."playerId" = p."hitterId" and
	 	rc."gamePk" = p."gamePk") "assist",
		
	(SELECT 
	 	count(*) filter (where(
			rc.credit = 'f_throwing_error' or
			rc.credit = 'f_error_dropped_ball' or
			rc.credit = 'f_fielding_error'
		))
	 FROM runner_credit rc 
	 where 
	  	rc."playerId" = p."hitterId" and
	 	rc."gamePk" = p."gamePk") "e",		
		
		
	(SELECT 
	 	count(*) filter (where(
			rc.credit = 'f_assist_of' 
		))
	 FROM runner_credit rc 
	 where 
	  	rc."playerId" = p."hitterId" and
	 	rc."gamePk" = p."gamePk") "ofAssist"	
	
from play p 
where 
	p.type = 'atBat'
group by 
	p."hitterId", p."gamePk"
limit 100