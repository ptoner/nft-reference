# Postgres

* Download and install postgres 13. https://www.postgresql.org/download/

* Schema migration https://github.com/salsita/node-pg-migrate
* Docs https://salsita.github.io/node-pg-migrate/#/


# Migrating Schema Changes

Documentation for library.
https://github.com/salsita/node-pg-migrate/blob/v3/docs/cli.md

1. Create nft_reference database in postgres.
2. Configuration in src/node-pg-migrate-config.json
3. To update local schema to latest state: npm run migrate up

Tests
1. Create test_nft_reference database in postgres.
2. Configuration in test/inversify.config.ts
3. To update local schema to latest state: npm run migrate:test up

# Install 

```console
npm install
```

# Run Tests

```console
npm run test
```

# Debugging Tests

An individual ts spec file can be debugged in VS Code:

* npm run watch
* Webpack is configured to compile to dist folder, produce source maps, and watch for changes.
* Select a ts spec file, i.e. game-dao.spec.ts.
* Add a breakpoint.
* Press F5 to run tests in the selected file.



# BigQuery (THIS WILL COME LATER)

Pricing
https://cloud.google.com/bigquery/pricing

Working with JSON in BigQuery
https://www.holistics.io/blog/how-to-extract-nested-or-array-json-in-bigquery/

BigQuery Samples
https://github.com/googleapis/nodejs-bigquery/tree/6438df629e9adb707338bd53959c7c10e0ae2936/samples


# Google Cloud Storage

Samples
https://github.com/googleapis/nodejs-storage/tree/master/samples